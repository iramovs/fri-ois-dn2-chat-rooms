var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var uporabnikiGledeKanal = [];

exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj');
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPosredovanjeZasebno(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    //obdelajPridruzitevKanaluZGeslom(socket);
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
      //console.log("zahtevek za posodobitev na kanalu");
      
      var kanal = trenutniKanal[socket.id];
      trenutniUporabniki(kanal, socket);
      socket.broadcast.to(kanal).emit("uporabnikiNaKanalu", {tabela: uporabnikiGledeKanal});
      socket.emit("uporabnikiNaKanalu", {tabela: uporabnikiGledeKanal}); //sebi
      
    });
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    
    socket.on("kanali", function(taKanal) {
      
    });
    
  });
};

function trenutniUporabniki(kanal) {
  //console.log(socket);
  uporabnikiGledeKanal = [];
  for (var i = 0; i < io.sockets.clients(kanal).length; i++) {
    var uporabnikSocketId = io.sockets.clients(kanal)[i].id;
    uporabnikiGledeKanal.push(vzdevkiGledeNaSocket[uporabnikSocketId]);
    //console.log("push");
  }
  
  return uporabnikiGledeKanal;
}

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}

function pridruzitevKanalu(socket, kanal) {
  socket.join(kanal);
  trenutniKanal[socket.id] = kanal;
  socket.emit('pridruzitevOdgovor', {kanal: kanal, nick: vzdevkiGledeNaSocket[socket.id]});
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });

  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
  }
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek,
          trenutniKanal: trenutniKanal[socket.id]
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function obdelajPosredovanjeZasebno(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on("zasebnoZahteva", function(rezultat) {
    
    var prejemnik = rezultat.prejemnik;
    var notranjost = rezultat.notranjost;
    
    if (uporabljeniVzdevki.indexOf(prejemnik) == -1 || 
      vzdevkiGledeNaSocket[socket.id] == prejemnik) {
      socket.emit("zasebnoZahtevaOdgovor", {
        uspesno: "false",
        sporocilo: "Sporočilo \""+notranjost+"\" uporabniku z vzdevkom \""+prejemnik+"\" ni bilo mogoče posredovati."
      });
      
    } else {
      socket.emit("zasebnoZahtevaOdgovor", {
        uspesno: "true",
        sporocilo: "(zasebno za "+prejemnik+"): "+notranjost
      });
      
      for (var i in vzdevkiGledeNaSocket) {
        if (vzdevkiGledeNaSocket[i] == prejemnik) {
          io.sockets.socket(i).emit("zasebnoZahtevaOdgovor", {
            uspesno: "sporocilo",
            sporocilo: vzdevkiGledeNaSocket[socket.id]+" (zasebno): "+notranjost
          });
        }
      }
      
      
    }
    
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo
    });
  });
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
    socket.leave(trenutniKanal[socket.id]);
    pridruzitevKanalu(socket, kanal.novKanal);
  });
}

function obdelajPridruzitevKanaluZGeslom(socket) {
  socket.on("pridruzitevZahtevaZGeslom", function(novi) {

    if (pridruzitevKanaluZGeslom(socket, novi.novKanal, novi.novGeslo)) {
      socket.leave(trenutniKanal[socket.id]);
    } else {
      
      console.log("nisem leavou");
      
    }

  });
}

function pridruzitevKanaluZGeslom(socket, kanal, geslo) {
  //socket.join(kanal);
  //kanal.preveriGeso();
  
  /*if (trenutniKanal[socket.id].geslo == "1") {
    trenutniKanal[socket.id] = kanal;
    socket.leave(kanal);
    return false;
  }*/
  
  return true;
  
}

function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
}