var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};

Klepet.prototype.spremeniKanalZGeslom = function(kanal, geslo) {
  this.socket.emit("pridruzitevZahtevaZGeslom", {
    novKanal: kanal,
    novGeslo: geslo
  });
}

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      besede = besede.join(' ');
      besede = besede.split("\"");
      
      if (besede.length == 1) {
        this.spremeniKanal(kanal);
        break;
      } else if (besede.length == 5) {
        kanal = besede[1];
        var geslo = besede[3];
        this.spremeniKanalZGeslom(kanal, geslo);
        break;
        
      } else {
        sporocilo = "Napacno formuliranje ukaza za pridruzitev kanala z geslom!"
        break;
      }
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
      
    case 'zasebno':
      besede.shift();
      besede = besede.join(' ');
      besede = besede.split('\"');
      
      if (besede.length != 5) {
        sporocilo = "Zasebno sporocilo je napacno formulirano.";
        break;
      }
      
      var prejemnik = besede[1];
      var notranjost = besede[3];

      this.socket.emit("zasebnoZahteva", {
        prejemnik: prejemnik,
        notranjost: notranjost
      });
      
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};