function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold">'+sporocilo+'</div>');
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    sporocilo = filtriraj(sporocilo);
    var kanal = $('#kanal').text();
    kanal = kanal.substring(kanal.indexOf("@ ")+2, kanal.length);
    klepetApp.posljiSporocilo(kanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

function odpriDatoteko() {
  var XML = null;
  
  $.ajax({
    type: "get",
    url: "./swearWords.xml",
    async: false,
    dataType: "xml",
    success: function(data) {
      XML = data;
    }
  });
  
  return XML;
}

function filtriraj(msg) {
  
  var cisto = "";
  var seznamKletvic = odpriDatoteko().getElementsByTagName("word");
  msg = msg.replace(/\./g, " .");
  msg = msg.replace(/\?/g, " ?");
  msg = msg.replace(/\!/g, " !");
  msg = msg.replace(/\,/g, " ,");
  var seznamBesed = msg.split(" ");
  
  for (var i = 0; i < seznamBesed.length; i++) {
    for (var j = 0; j < seznamKletvic.length; j++) {
      
      var enaki = seznamBesed[i].toUpperCase() == seznamKletvic[j].innerHTML.toUpperCase();
      if (enaki) {
        //console.log("cenzurirano");
        seznamBesed[i] = seznamBesed[i].replace(/./g, "*");
      }
    }
    cisto = cisto+seznamBesed[i]+" ";
  }
  cisto = cisto.replace(/\s\./g, ".");
  cisto = cisto.replace(/\s\?/g, "?");
  cisto = cisto.replace(/\s\!/g, "!");
  cisto = cisto.replace(/\s\,/g, ",");
  var sporocilo = cisto.replace(/</g, '&lt');
  sporocilo = sporocilo.replace(/>/g, '&gt');
  sporocilo = sporocilo.replace(/;\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png"/>');
  sporocilo = sporocilo.replace(/:\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png"/>');
  sporocilo = sporocilo.replace(/\(y\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png"/>');
  sporocilo = sporocilo.replace(/:\*/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png"/>');
  sporocilo = sporocilo.replace(/:\(/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png"/>');
  return sporocilo;
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      var vzdevek = rezultat.vzdevek;
      var kanal = rezultat.trenutniKanal;
      $("#kanal").text(vzdevek+" @ "+kanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  socket.on("zasebnoZahtevaOdgovor", function(rezultat) {
    
    var sporocilo;
    if (rezultat.uspesno == "true") {
      sporocilo = rezultat.sporocilo;
      sporocilo = filtriraj(sporocilo);
      $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    } else if (rezultat.uspesno == "false") {
      sporocilo = rezultat.sporocilo;
      sporocilo = filtriraj(sporocilo);
      $('#sporocila').append(divElementHtmlTekst(sporocilo));
    } else if (rezultat.uspesno == "sporocilo") {
      sporocilo = rezultat.sporocilo;
      sporocilo = filtriraj(sporocilo);
      $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    }
    
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    var vzdevek = rezultat.nick;
    $('#kanal').text(vzdevek+" @ "+rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold">'+sporocilo.besedilo+'</div>');
    $('#sporocila').append(novElement);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  socket.on("uporabnikiNaKanalu", function(rezultat) {
    
    $('#seznam-uporabnikov').empty();
    //console.log(rezultat.tabela);
    
    for (var i = 0; i < rezultat.tabela.length; i++) {
       $('#seznam-uporabnikov').append(divElementEnostavniTekst(rezultat.tabela[i]));
    }
    
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});